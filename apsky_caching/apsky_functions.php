<?php
function apsky_caching(){
    global $wpdb;
    $url = get_permalink();
    $results = $wpdb->get_results( 'SELECT * FROM apsky_cache WHERE url = "'.$url.'"', OBJECT );
    $category = get_the_category();
    if(empty($category)){
        $priority = 'medium';
    }else{
        $cat_id = $category[0]->term_id;
        $post_pri = $wpdb->get_results( 'SELECT * FROM wp_options WHERE option_name = "apsky_pri_cat_'.$cat_id.'"', OBJECT );
        $priority = $post_pri[0]->option_value;
    }
    $options = $wpdb->get_results( 'SELECT * FROM wp_options WHERE option_name = "apsky_'.$priority.'"', OBJECT );
    $timeout = $options[0]->option_value;
    $cachefile = __DIR__."/cache/".$results[0]->file.".html";
    $created = new DateTime($results[0]->created);
    $now = new DateTime(current_time( 'mysql' ));
    $interval = $now->diff($created);
    $seconds = $interval->s + $interval->i*60 + $interval->h*60*60 + $interval->d*24*60*60;
    if (file_exists($cachefile) && $seconds < $timeout) {
        include($cachefile); 
        exit;
    }else{
        ob_start();
    }
}

function apsky_cache_save(){
    global $wpdb;
    if(get_permalink()){
        $url = get_permalink();
    }else{
        $url = 'empty_url';
    }
    $results = $wpdb->get_results( 'SELECT * FROM apsky_cache WHERE url = "'.$url.'"', OBJECT );
    $id = $results[0]->id;
    $file = str_replace(["/", ":_"], ["_", ""], $url);
    $cachefile = __DIR__."/cache/".$file.".html";
    // open the cache file for writing
    $fp = fopen($cachefile, 'w'); 
    // save the contents of output buffer to the file
    fwrite($fp, ob_get_contents());
    // close the file
    fclose($fp);
    
    if($id){
        apsky_update($url, $file, $id);
    }else{
        apsky_insert($url, $file);
    }
    // Send the output to the browser
    ob_end_flush(); 
}

global $apsky_db_version;
$apsky_db_version = '1.0';

function apsky_install() {
    global $wpdb;
    global $apsky_db_version;

    $table_name = 'apsky_cache';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            url text DEFAULT '' NOT NULL,
            file text NOT NULL,
            priority text DEFAULT '' NOT NULL,
            created datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );

    add_option( 'apsky_db_version', $apsky_db_version );
}

function apsky_deactivate(){
    $option_name1 = 'apsky_high';
    $option_name2 = 'apsky_medium';
    $option_name3 = 'apsky_low';

    delete_option($option_name1);
    delete_option($option_name2);
    delete_option($option_name3);

    // for site options in Multisite
    delete_site_option($option_name);

    // drop a custom database table
    global $wpdb;
    $wpdb->query("DROP TABLE IF EXISTS apsky_cache");
}

function apsky_insert($url, $file) {
	global $wpdb;
	$table_name = 'apsky_cache';
        $category = get_the_category();
        if(empty($category)){
            $priority = 'medium';
        }else{
            $cat_id = $category[0]->term_id;
            $post_pri = $wpdb->get_results( 'SELECT * FROM wp_options WHERE option_name = "apsky_pri_cat_'.$cat_id.'"', OBJECT );
            $priority = $post_pri[0]->option_value;
        }
	$wpdb->insert( 
		$table_name, 
		array( 
                    'url' => $url,
                    'file' => $file, 
                    'created' => current_time( 'mysql' ),
                    'priority' => $priority,
		) 
	);
}

function apsky_update($url, $file, $id) {
	global $wpdb;
	$table_name = 'apsky_cache';
        $category = get_the_category();
        if(empty($category)){
            $priority = 'medium';
        }else{
            $cat_id = $category[0]->term_id;
            $post_pri = $wpdb->get_results( 'SELECT * FROM wp_options WHERE option_name = "apsky_pri_cat_'.$cat_id.'"', OBJECT );
            $priority = $post_pri[0]->option_value;
        }
	$wpdb->update( 
		$table_name, 
		array( 
                    'url' => $url,
                    'file' => $file, 
                    'created' => current_time( 'mysql' ),
                    'priority' => $priority,
		),
                array(
                    'id' => $id,
                )
	);
}

function apsky_db_update() {
    global $apsky_db_version;
    if ( get_site_option( 'apsky_db_version' ) != $apsky_db_version ) {
        apsky_install();
    }
}


function apsky_menu() {

	//create new top-level menu
	add_menu_page('Caching Settings',  __('Cache Settings', 'apsky_caching'), 'administrator', __FILE__, 'apsky_settings_page' , 'dashicons-admin-generic' );

}


function apsky_init() {
	//register our settings
	register_setting( 'apsky-settings-group', 'apsky_high' );
        register_setting( 'apsky-settings-group', 'apsky_medium' );
        register_setting( 'apsky-settings-group', 'apsky_low' );
}

function apsky_settings_page() {
    global $wpdb;
    $results = $wpdb->get_results( 'SELECT * FROM apsky_cache WHERE 1', OBJECT );
    $now = new DateTime(current_time( 'mysql' ));
    $option1 = $wpdb->get_results( 'SELECT * FROM wp_options WHERE option_name = "apsky_high"', OBJECT );
    $option2 = $wpdb->get_results( 'SELECT * FROM wp_options WHERE option_name = "apsky_medium"', OBJECT );
    $option3 = $wpdb->get_results( 'SELECT * FROM wp_options WHERE option_name = "apsky_low"', OBJECT );
    $timeout1 = $option1[0]->option_value;
    $timeout2 = $option2[0]->option_value;
    $timeout3 = $option3[0]->option_value;
    ?>
      
<div class="wrap">
<h1>Caching</h1>

<form method="post" action="options.php">
    <?php settings_fields( 'apsky-settings-group' ); ?>
    <?php do_settings_sections( 'apsky-settings-group' ); ?>
    <table>
        <tr>
            <td>
                <label for="apsky_high"><?php _e('High Priority', 'apsky_caching')?></label>
            </td>
            <td>
                <label for="apsky_medium"><?php _e('Medium Priority', 'apsky_caching')?></label>
            </td>
            <td>
                <label for="apsky_low"><?php _e('Low Priority', 'apsky_caching')?></label>
            </td>
        </tr>
        <tr>
            <td>
                <input id="apsky_high" type="number" name="apsky_high" value="<?php echo ( get_option('apsky_high') ? esc_attr( get_option('apsky_high') ) : 3600 ); ?>" /> <?php _e('sec.', 'apsky_caching')?>
            </td>
            <td>
                <input id="apsky_medium" type="number" name="apsky_medium" value="<?php echo ( get_option('apsky_medium') ? esc_attr( get_option('apsky_medium') ) : 60 ); ?>" /> <?php _e('sec.', 'apsky_caching')?>
            </td>
            <td>
                <input id="apsky_low" type="number" name="apsky_low" value="<?php echo ( get_option('apsky_low') ? esc_attr( get_option('apsky_low') ) : 0 ); ?>" /> <?php _e('sec.', 'apsky_caching')?>
            </td>
        </tr>
    </table>
        
    <?php submit_button(); ?>

</form>

<table>
    <th><?php _e('URL', 'apsky_caching')?></th>
    <th><?php _e('File', 'apsky_caching')?></th>
    <th><?php _e('Created', 'apsky_caching')?></th>
    <th><?php _e('Expiration', 'apsky_caching')?></th>
    <th><?php _e('Priority', 'apsky_caching')?></th>
    <?php
foreach ($results as $result){
    if($result->priority == 'high'){
        $timeout = $timeout1;
    }elseif ($result->priority == 'medium') {
        $timeout = $timeout2;   
    }else{
        $timeout = $timeout3;
    }
    ?>
    <tr>
        <td style="
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 50ch;">
            <?php echo $result->url;?>
        </td>
        <td style="
            padding: 0 10px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 50ch;">
            <?php echo $result->file.'.html';?>
        </td>
        <td style="
            padding: 0 10px;
            text-align: center;">
                <?php echo $result->created;?>
        </td>
        <?php 
            $created = new DateTime($result->created);
            $interval = $now->diff($created);
            $seconds = $interval->s + $interval->i*60 + $interval->h*60*60 + $interval->d*24*60*60;
        ?>
        <td style="
            padding: 0 10px;
            text-align: center;"><?php
        if($timeout-$seconds>0){
            echo $timeout-$seconds.' '.__('sec.', 'apsky_caching');
        }else{
            _e('is expired', 'apsky_caching');
        }
        ?></td>
        <td style="
            padding: 0 10px;
            text-align: center;">
            <?php echo ($result->priority == 'high')? __('high', 'apsky_caching') : ( ($result->priority == 'medium') ? __('medium', 'apsky_caching') : __('low', 'apsky_caching') ); ?>
        </td>
    </tr>
    <?php
        }
    ?>
</table>
</div>
<?php 
} 

// save extra category extra fields callback function
function save_added_category_fileds( $term_id ) {
    if ( isset( $_POST['apsky_cat_priority'] ) ) {
        $t_id = $term_id;
            
        $cat_meta = $_POST['apsky_cat_priority'];
          
        //save the option array
        update_option( "apsky_pri_cat_$t_id", $cat_meta );
    }
}
function save_edited_category_fileds( $term_id ) {
    if ( isset( $_POST['apsky_cat_priority'] ) ) {
        $t_id = $term_id;
            
        $cat_meta = $_POST['apsky_cat_priority'];
          
        //save the option array
        update_option( "apsky_pri_cat_$t_id", $cat_meta );
    }
}

//add extra fields to category edit form callback function
function apsky_add_category_fields() {
    ?>
    
    <tr class="form-field">
        <th scope="row" valign="top"><label for="apsky_cat_priority"><?php _e('Category Priority', 'apsky_caching'); ?></label></th>
        <td>
            <select name="apsky_cat_priority" id="apsky_cat_priority">
                <option value="high"><?php _e('High', 'apsky_caching'); ?></option>
                <option value="medium" selected ><?php _e('Medium', 'apsky_caching'); ?></option>
                <option value="low"><?php _e('Low', 'apsky_caching'); ?></option>
            </select>
            <br />
            <span class="description"><?php _e('Category Priority: select priority', 'apsky_caching'); ?></span>
        </td>
    </tr>
    
    <?php
}

function apsky_edit_category_fields($cat) {    //check for existing featured ID
$category = $cat;
$cat_id = $category->term_id;
global $wpdb;
	
$cat_pri = $wpdb->get_row( 'SELECT * FROM wp_options WHERE option_name = "apsky_pri_cat_'.$cat_id.'"', OBJECT );
$priority = $cat_pri->option_value;
    ?>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="apsky_cat_priority"><?php _e('Category Priority', 'apsky_caching'); ?></label></th>
        <td>
            <select name="apsky_cat_priority" id="apsky_cat_priority">
                <option value="high" <?php echo ($priority == 'high') ? 'selected' : '' ?>><?php _e('High', 'apsky_caching'); ?></option>
                <option value="medium" <?php echo ($priority == 'medium' || !$priority) ? 'selected' : '' ?>><?php _e('Medium', 'apsky_caching'); ?></option>
                <option value="low" <?php echo ($priority == 'low') ? 'selected' : '' ?>><?php _e('Low', 'apsky_caching'); ?></option>
            </select>
            <br />
            <span class="description"><?php _e('Category Priority: select priority', 'apsky_caching'); ?></span>
        </td>
    </tr>
    
    <?php
}

function apsky_load_textdomain() {
    $plugin_dir = dirname( plugin_basename(__FILE__) ). '/languages/' ;
    load_plugin_textdomain( 'apsky_caching', false, $plugin_dir );
}
