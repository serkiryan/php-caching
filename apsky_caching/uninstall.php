<?php
// if uninstall.php is not called by WordPress, die
if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}
 
$option_name1 = 'apsky_high';
$option_name2 = 'apsky_medium';
$option_name3 = 'apsky_low';
 
delete_option($option_name1);
delete_option($option_name2);
delete_option($option_name3);
 
 
// drop a custom database table
global $wpdb;
$wpdb->query("DROP TABLE IF EXISTS apsky_cache");
