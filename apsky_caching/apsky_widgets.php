<?php
class apsky_caching_widget extends WP_Widget{
    function __construct() {
        parent::__construct(
            'apsky_caching_widget',
            __('PHP Caching Widget', 'apsky_caching'),
            array('Description' => __('Widget for PHP Caching plugin', 'apsky_caching'))
        );
    }
    
    
    public function widget($args, $instance) {
        $title = apply_filters('widget_title', $instance['title']);
        echo $args['before_widget'];
        if (!empty($title)){
            echo $args['before_title'] . $title . $args['after_title'];
        }
            global $wpdb;
            $posts = get_posts();
            $pages = get_pages();
            
            ?>

        <div class="">
        <h5><?php _e('Pages', 'apsky_caching');?></h5>
        <table>
            <?php
        foreach ($pages as $page){
            ?>
            <tr>
                <td style="
                    white-space: nowrap;
                    overflow: hidden;
                    text-overflow: ellipsis;
                    max-width: 50ch;">
                    <a href="<?php echo $page->guid;?>" target="_blank"><?php echo $page->post_title;?></a>
                </td>
            </tr>
            <?php
                }
            ?>
        </table>
        
        <h5><?php _e('Posts', 'apsky_caching');?></h5>
        <table>
            <?php
        foreach ($posts as $post){
            ?>
            <tr>
                <td style="
                    white-space: nowrap;
                    overflow: hidden;
                    text-overflow: ellipsis;
                    max-width: 50ch;">
                    <a href="<?php echo $post->guid;?>" target="_blank"><?php echo $post->post_title;?></a>
                </td>
            </tr>
            <?php
                }
            ?>
        </table>
        </div>
        <?php 
        wp_reset_query();
        echo $args['after_widget'];
    }
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('PHP Cache', 'apsky_caching');
        }
        ?>
            <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
        <?php
    }
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }
}
function apsky_load_widget() {
	register_widget( 'apsky_caching_widget' );
}
add_action( 'widgets_init', 'apsky_load_widget' );