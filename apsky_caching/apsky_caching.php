<?php
/*
  Plugin Name: PHP Caching
  Plugin URI:
  Description: PHP Caching
  Version: 1.0
  Author: Sergey Kirakosyan
  Author URI:
  Text Domain: apsky_caching
  Domain Path: /languages/
 */

require_once(__DIR__ . '/apsky_functions.php');
require_once(__DIR__.'/apsky_widgets.php');

add_action('wp', 'apsky_caching');
add_action('wp_footer', 'apsky_cache_save');
add_action('plugins_loaded', 'apsky_db_update');

register_activation_hook(__FILE__, 'apsky_install');
register_activation_hook(__FILE__, 'apsky_install_data');
register_deactivation_hook(__FILE__, 'apsky_deactivate');

if (is_admin()) {
    add_action('admin_menu', 'apsky_menu');
    add_action('admin_init', 'apsky_init');
}

//add extra fields to category edit form hook
add_action('category_add_form_fields', 'apsky_add_category_fields');
add_action('category_edit_form_fields', 'apsky_edit_category_fields');

// save extra category extra fields hook
add_action ( 'create_category', 'save_added_category_fileds');
add_action ( 'edited_category', 'save_edited_category_fileds');

add_action('plugins_loaded', 'apsky_load_textdomain');

